package Stream;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class MyStream<T> {

    private final Generator<T> generator;
    private static long count;
    private MyStream(Generator<T> generator) {
        this.generator = generator;
    }


    public MyStream<T> filter(Predicate<T> predicate) {
        return new MyStream<>(generatorContext -> generator.generate(value -> {
            if (predicate.test(value)) {
                generatorContext.next(value);
            }
        }));
    }

    public <R> MyStream<R> map(Function<T, R> function) {
        return new MyStream<>(generatorContext -> generator.generate(
                value -> generatorContext.next(function.apply(value))
        ));
    }

    public MyStream<T> limit(long maxSize) {
        if (maxSize < 0)
            throw new IllegalArgumentException(Long.toString(maxSize));
        return new MyStream<>(generatorContext -> generator.generate(value -> {
            count++;
            if(count <= maxSize){
                generatorContext.next(value);
            }
        }));
    }

    public MyStream<T> dictinct() {
        Set<T> distinctSet = new LinkedHashSet<>();
        return new MyStream<>(generatorContext -> generator.generate(value -> {
            if(!distinctSet.contains(value)){
                generatorContext.next(value);
            }
            distinctSet.add(value);
        }));
    }


    public List<T> toList(){
        List<T> variables = new ArrayList<>();
        generator.generate(variables::add);
        return variables;
    }

    public long count(){
        generator.generate(value -> {
            count++;
        });
        return count;
    }

    public <K, U> Map<K,U> toMap(Function<? super T, ? extends K> keyMapper,
                                    Function<? super T, ? extends U> valueMapper) {
        Map<K, U> map = new HashMap<>();
        generator.generate(value -> {
            map.put(keyMapper.apply(value), valueMapper.apply(value));
        });
        return map;
    }

    public void forEach(Consumer<T> consumer) {
        generator.generate(consumer::accept);
    }

    public static <T> MyStream<T> of(Collection<T> collection) {
        return new MyStream<>(generatorContext ->
                collection.forEach(generatorContext::next)
        );
    }

}