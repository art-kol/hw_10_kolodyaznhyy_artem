package Stream;
import java.util.*;
public class Main {
    public static void main(String... args) {

        Apple apple1 = new Apple("green", 10);
        Apple apple2 = new Apple("green", 10);
        Apple apple3 = new Apple("red", 9);
        Apple apple4 = new Apple("green", 7);
        Apple apple5 = new Apple("red", 12);
        Apple apple6 = new Apple("red", 15);
        Apple apple7 = new Apple("green", 16);

        List<Apple> apples = Arrays.asList(apple1, apple2, apple3, apple4, apple5, apple6, apple7);

        System.out.println("filter");
        MyStream.of(apples).filter(s -> s.getWeight()>9).forEach(System.out::println);
        System.out.println("filter+limit");
        MyStream.of(apples).filter(s -> s.getWeight()>9).limit(3).forEach(System.out::println);
        System.out.println("filter+distinct");
        MyStream.of(apples).filter(s->s.getColor().equals("green")).dictinct().forEach(System.out::println);
        System.out.println("map");
        MyStream.of(apples).map(s-> s + " CHECK MAP").forEach(System.out::println);
        System.out.println("filter+toList");
        List<Apple> redApples = MyStream.of(apples).filter(s -> s.getColor().equals("red")).toList();
        System.out.println(redApples);
        System.out.println("toMap");
        Map redApplesMap = MyStream.of(apples).toMap(Apple::getWeight, Apple::getColor);;
        System.out.println(redApplesMap);

    }
}
