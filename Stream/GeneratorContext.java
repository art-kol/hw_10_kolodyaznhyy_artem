package Stream;

@FunctionalInterface
public interface GeneratorContext<T> {
    void next(T value);
}
