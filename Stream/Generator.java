package Stream;

@FunctionalInterface
public interface Generator<T> {
    void generate(GeneratorContext<T> context);
}